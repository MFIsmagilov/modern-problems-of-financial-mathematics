# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'MainWindow.ui'
#
# Created by: PyQt5 UI code generator 5.6
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(592, 447)
        MainWindow.setStyleSheet("QPushButton {\n"
"    background-color:#ffec64;\n"
"    border-radius:4px;\n"
"    border:1px solid #ffaa22;\n"
"    color:#333333;\n"
"    font-family:\"Segoe UI\";\n"
"    font-size:14px;\n"
"    height:30px;\n"
"    line-height:30px;\n"
"    width:90px;\n"
"    text-decoration:none;\n"
"    text-align:center;\n"
"}\n"
"QPushButton:disabled{\n"
"    background-color:#cb881b;\n"
"    border:1px solid #bd9009;\n"
"}\n"
"\n"
"QPushButton:hover {\n"
"    background-color:#ffab23;\n"
"}\n"
"\n"
"QGroupBox{\n"
"    font-family:\"Segoe UI\";\n"
"    font-size:16px;\n"
"}\n"
"QLineEdit{\n"
"    font-family:\"Segoe UI\";\n"
"}\n"
"QLabel{\n"
"    font-family:\"Segoe UI\";\n"
"    font-size:14px;\n"
"}")
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout(self.centralwidget)
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.tabWidget = QtWidgets.QTabWidget(self.centralwidget)
        self.tabWidget.setObjectName("tabWidget")
        self.OptionsComputing = QtWidgets.QWidget()
        self.OptionsComputing.setObjectName("OptionsComputing")
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout(self.OptionsComputing)
        self.horizontalLayout_3.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        self.frame_3 = QtWidgets.QFrame(self.OptionsComputing)
        self.frame_3.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame_3.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_3.setObjectName("frame_3")
        self.gridLayout_3 = QtWidgets.QGridLayout(self.frame_3)
        self.gridLayout_3.setObjectName("gridLayout_3")
        self.frame = QtWidgets.QFrame(self.frame_3)
        self.frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame.setObjectName("frame")
        self.gridLayout = QtWidgets.QGridLayout(self.frame)
        self.gridLayout.setObjectName("gridLayout")
        self.B0_edit = QtWidgets.QLineEdit(self.frame)
        self.B0_edit.setObjectName("B0_edit")
        self.gridLayout.addWidget(self.B0_edit, 4, 1, 1, 1)
        self.r_edit = QtWidgets.QLineEdit(self.frame)
        self.r_edit.setObjectName("r_edit")
        self.gridLayout.addWidget(self.r_edit, 2, 1, 1, 1)
        self.B0_label = QtWidgets.QLabel(self.frame)
        self.B0_label.setObjectName("B0_label")
        self.gridLayout.addWidget(self.B0_label, 4, 0, 1, 1)
        self.K_label = QtWidgets.QLabel(self.frame)
        self.K_label.setObjectName("K_label")
        self.gridLayout.addWidget(self.K_label, 3, 0, 1, 1)
        self.K_edit = QtWidgets.QLineEdit(self.frame)
        self.K_edit.setObjectName("K_edit")
        self.gridLayout.addWidget(self.K_edit, 3, 1, 1, 1)
        self.r_label = QtWidgets.QLabel(self.frame)
        self.r_label.setObjectName("r_label")
        self.gridLayout.addWidget(self.r_label, 2, 0, 1, 1)
        self.S0_label = QtWidgets.QLabel(self.frame)
        self.S0_label.setObjectName("S0_label")
        self.gridLayout.addWidget(self.S0_label, 5, 0, 1, 1)
        self.a_label = QtWidgets.QLabel(self.frame)
        self.a_label.setObjectName("a_label")
        self.gridLayout.addWidget(self.a_label, 0, 0, 1, 1)
        self.b_label = QtWidgets.QLabel(self.frame)
        self.b_label.setObjectName("b_label")
        self.gridLayout.addWidget(self.b_label, 1, 0, 1, 1)
        self.a_edit = QtWidgets.QLineEdit(self.frame)
        self.a_edit.setObjectName("a_edit")
        self.gridLayout.addWidget(self.a_edit, 0, 1, 1, 1)
        self.N_edit = QtWidgets.QLineEdit(self.frame)
        self.N_edit.setObjectName("N_edit")
        self.gridLayout.addWidget(self.N_edit, 6, 1, 1, 1)
        self.N_label = QtWidgets.QLabel(self.frame)
        self.N_label.setObjectName("N_label")
        self.gridLayout.addWidget(self.N_label, 6, 0, 1, 1)
        self.S0_edit = QtWidgets.QLineEdit(self.frame)
        self.S0_edit.setObjectName("S0_edit")
        self.gridLayout.addWidget(self.S0_edit, 5, 1, 1, 1)
        self.b_edit = QtWidgets.QLineEdit(self.frame)
        self.b_edit.setObjectName("b_edit")
        self.gridLayout.addWidget(self.b_edit, 1, 1, 1, 1)
        self.groupBox = QtWidgets.QGroupBox(self.frame)
        self.groupBox.setObjectName("groupBox")
        self.gridLayout_2 = QtWidgets.QGridLayout(self.groupBox)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.drop_button = QtWidgets.QPushButton(self.groupBox)
        self.drop_button.setMinimumSize(QtCore.QSize(91, 41))
        self.drop_button.setObjectName("drop_button")
        self.gridLayout_2.addWidget(self.drop_button, 0, 0, 1, 1)
        self.rise_button = QtWidgets.QPushButton(self.groupBox)
        self.rise_button.setMinimumSize(QtCore.QSize(91, 41))
        self.rise_button.setObjectName("rise_button")
        self.gridLayout_2.addWidget(self.rise_button, 0, 1, 1, 1)
        self.gridLayout.addWidget(self.groupBox, 8, 1, 1, 1)
        self.start_button = QtWidgets.QPushButton(self.frame)
        self.start_button.setMinimumSize(QtCore.QSize(0, 41))
        self.start_button.setObjectName("start_button")
        self.gridLayout.addWidget(self.start_button, 7, 1, 1, 1)
        self.gridLayout_3.addWidget(self.frame, 0, 0, 1, 1)
        self.frame_2 = QtWidgets.QFrame(self.frame_3)
        self.frame_2.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame_2.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_2.setObjectName("frame_2")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.frame_2)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.result_textedit = QtWidgets.QTextEdit(self.frame_2)
        self.result_textedit.setMinimumSize(QtCore.QSize(0, 0))
        self.result_textedit.setObjectName("result_textedit")
        self.horizontalLayout.addWidget(self.result_textedit)
        self.gridLayout_3.addWidget(self.frame_2, 0, 1, 1, 1)
        self.horizontalLayout_3.addWidget(self.frame_3)
        self.tabWidget.addTab(self.OptionsComputing, "")
        self.history_tab = QtWidgets.QWidget()
        self.history_tab.setObjectName("history_tab")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.history_tab)
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout.setObjectName("verticalLayout")
        self.frame_4 = QtWidgets.QFrame(self.history_tab)
        self.frame_4.setMinimumSize(QtCore.QSize(0, 50))
        self.frame_4.setMaximumSize(QtCore.QSize(16777215, 255))
        self.frame_4.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame_4.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_4.setObjectName("frame_4")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(self.frame_4)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.frame_6 = QtWidgets.QFrame(self.frame_4)
        self.frame_6.setMaximumSize(QtCore.QSize(16777215, 55))
        self.frame_6.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame_6.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_6.setObjectName("frame_6")
        self.formLayout = QtWidgets.QFormLayout(self.frame_6)
        self.formLayout.setObjectName("formLayout")
        self.countIteration_label = QtWidgets.QLabel(self.frame_6)
        self.countIteration_label.setObjectName("countIteration_label")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.countIteration_label)
        self.countIterationValue_label = QtWidgets.QLabel(self.frame_6)
        self.countIterationValue_label.setObjectName("countIterationValue_label")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.countIterationValue_label)
        self.verticalLayout_2.addWidget(self.frame_6)
        self.verticalLayout.addWidget(self.frame_4)
        self.frame_5 = QtWidgets.QFrame(self.history_tab)
        self.frame_5.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame_5.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_5.setObjectName("frame_5")
        self.horizontalLayout_4 = QtWidgets.QHBoxLayout(self.frame_5)
        self.horizontalLayout_4.setObjectName("horizontalLayout_4")
        self.history_textEdit = QtWidgets.QTextEdit(self.frame_5)
        self.history_textEdit.setObjectName("history_textEdit")
        self.horizontalLayout_4.addWidget(self.history_textEdit)
        self.verticalLayout.addWidget(self.frame_5)
        self.tabWidget.addTab(self.history_tab, "")
        self.horizontalLayout_2.addWidget(self.tabWidget)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 592, 21))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        self.tabWidget.setCurrentIndex(0)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "Опционы европейского типа на биномиальном (B, S)-рынке"))
        self.B0_label.setText(_translate("MainWindow", "B0"))
        self.K_label.setText(_translate("MainWindow", "K"))
        self.r_label.setText(_translate("MainWindow", "r"))
        self.S0_label.setText(_translate("MainWindow", "S0"))
        self.a_label.setText(_translate("MainWindow", "a"))
        self.b_label.setText(_translate("MainWindow", "b"))
        self.N_label.setText(_translate("MainWindow", "N"))
        self.groupBox.setTitle(_translate("MainWindow", "Цены:"))
        self.drop_button.setText(_translate("MainWindow", "Упали"))
        self.rise_button.setText(_translate("MainWindow", "Поднялись"))
        self.start_button.setText(_translate("MainWindow", "Старт"))
        self.result_textedit.setHtml(_translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'MS Shell Dlg 2\'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p></body></html>"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.OptionsComputing), _translate("MainWindow", "Вычисления"))
        self.countIteration_label.setText(_translate("MainWindow", "Количество шагов:"))
        self.countIterationValue_label.setText(_translate("MainWindow", "0"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.history_tab), _translate("MainWindow", "История"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())

