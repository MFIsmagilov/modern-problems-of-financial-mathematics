from PyQt5.QtWidgets import QMainWindow

from GUI.view.Ui_MainWindow import Ui_MainWindow
from core.OptionsComputing import OptionsComputing, SharePriceBehavior


class MainWindow(QMainWindow, Ui_MainWindow):
	def __init__(self):
		QMainWindow.__init__(self)
		self.setupUi(self)
		self.setDefaultParams()
		self.oc = None

		self.start_button.clicked.connect(self.start_button_clicked)
		self.rise_button.clicked.connect(self.rise_button_clicked)
		self.drop_button.clicked.connect(self.drop_button_clicked)

	def setDefaultParams(self):
		self.a_edit.setText(str(-0.4))
		self.b_edit.setText(str(0.2))
		self.r_edit.setText(str(0))
		self.K_edit.setText(str(150.0))
		self.B0_edit.setText(str(1.0))
		self.S0_edit.setText(str(150.0))
		self.N_edit.setText(str(1))

	def start_button_clicked(self):
		a = float(self.a_edit.text())
		b = float(self.b_edit.text())
		r = float(self.r_edit.text())
		K = float(self.K_edit.text())
		B0 = float(self.B0_edit.text())
		S0 = float(self.S0_edit.text())
		N = int(self.N_edit.text())
		self.oc = OptionsComputing(a, b, r, K, B0, S0, N)

		self.result_textedit.setText(self.oc.getLastIteration().get_html_text())

	def rise_button_clicked(self):
		if self.oc is not None:
			value = self.oc.nextStep(SharePriceBehavior.RISE)
			self.result_textedit.setText(value.get_html_text())

	def drop_button_clicked(self):
		if self.oc is not None:
			value = self.oc.nextStep(SharePriceBehavior.DROP)
			self.result_textedit.setText(value.get_html_text())

	def on_tabWidget_currentChanged(self, index):

		if self.tabWidget.currentWidget().objectName() == 'history_tab' and self.oc is not None:
			self.countIterationValue_label.setText(str(self.oc.getIterationsCount()))
			self.history_textEdit.setText(self.oc.get_html_text())
