from enum import Enum
import math


class SharePriceBehavior(Enum):
	RISE = 1,
	DROP = 2,
	NOT_CHANGED = 3


class IterationValue:
	def __init__(self, gammaNext, bettaNext, ro, S, B, X, sharePriceBehavior, description):
		self.gammaNext = gammaNext
		self.bettaNext = bettaNext
		self.ro = ro
		self.S = S
		self.B = B
		self.X = X
		self.description = description
		self.sharePriceBehavior = sharePriceBehavior

	def __str__(self):
		return "{description}\n (B: {B}, S:{S}) (γ: {gammaNext}, β: {bettaNext})" \
			.format(description=self.description,
					B=self.B,
					S=self.S,
					gammaNext=self.gammaNext,
					bettaNext=self.bettaNext)

	def get_html_text(self):
		header = ""
		if self.sharePriceBehavior == SharePriceBehavior.NOT_CHANGED:
			header = "Инициализация:&nbsp;&nbsp;"
		elif self.sharePriceBehavior == SharePriceBehavior.RISE:
			header = "Цены поднялись:&nbsp;"
		elif self.sharePriceBehavior == SharePriceBehavior.DROP:
			header = "Цены упали:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
		text = """
	<table>
	    <tr>
	        <td>{header}</td>
	        <td></td>
	    </tr>
	    <tr>
	        <td></td>
	        <td>{description}</td>
	    </tr>
	    <tr></tr>
	    <tr>
	        <td></td>
	        <td>
	            <span style="font-weight: bold;">B</span>: {B}<br>
	            <span style="font-weight: bold;">S</span>: {S}<br>
	            <span style="font-weight: bold;">γ</span>: {gamma}<br>
	            <span style="font-weight: bold;">β</span>: {betta}<br>
	        </td>
	    </tr>
	</table>
			""".format(
			header = header,
			description = self.description,
			B = self.B,
			S = self.S,
			gamma = self.gammaNext,
			betta = self.bettaNext)
		return text

class OptionsComputing:
	def __init__(self, a, b, r, K, B0, S0, N):
		self.iterations = list()
		self.a = a
		self.b = b
		self.r = r
		self.K = K
		self.B0 = B0
		self.S0 = S0
		self.N = N

		self.p = (self.r - self.a) / (self.b - self.a)
		self.cn = self.CN(S0)

		self.zeroIteration()

	def zeroIteration(self):
		gamma = self.gamma_n(1, self.S0)
		betta = self.betta_n(1, self.S0)
		description = "Эмитент должен взять в долг облигации по цене {B0} на сумму {summa} и купить на эти деньги акции по цене {S0}".format(
			B0=self.B0,
			summa=(-betta) * self.B0,
			S0=self.S0)
		self.iterations.append(IterationValue(gamma, betta, 0.0, self.S0, self.B0, 0.0, SharePriceBehavior.NOT_CHANGED, description))

	def f(self, x):
		return max(0.0, x - self.K)

	def FN(self, n, S):
		sum = 0.0
		for k in range(0, n + 1):
			sum += self.binomialCoefficient(n, k) * \
				   pow(self.p, float(k)) * \
				   pow(1 - self.p, float(n - k)) * \
				   max(0.0, S * pow(1 + self.a, float(n)) * pow((1 + self.b) / (1 + self.a), float(k)) - self.K)
		return sum

	def Fn(self, n, x):
		sum = 0.0
		for k in range(0, n + 1):
			sum += self.f(x * pow(1 + self.b, float(k)) * pow(1 + self.a, float(n - k))) * \
				   self.binomialCoefficient(n, k) * pow(self.p, float(k)) * pow(1 - self.p, float(n - k))
		return sum

	def CN(self, S0):
		return pow(1 + self.r, float(-self.N)) * self.FN(self.N, S0)

	def SN(self, S_prev, ro_n):
		return (1 + ro_n) * S_prev

	def BN(self, n):
		return self.B0 * pow((1 + self.r), float(n))

	def gamma_n(self, n, S_prev):
		return pow(1 + self.r, float(-self.N + n)) * \
			   (self.Fn(self.N - n, S_prev * (1 + self.b)) - self.Fn(self.N - n, S_prev * (1 + self.a))) / (S_prev * (self.b - self.a))

	def betta_n(self, n, S_prev):
		return self.Fn(self.N - n + 1, S_prev) / self.BN(self.N) - (pow(1 + self.r, float(-self.N + n)) * (
		self.Fn(self.N - n,
				S_prev * (1 + self.b)) - self.Fn(
			self.N - n, S_prev * (1 + self.a)))) / (self.BN(n - 1) * (self.b - self.a))

	def binomialCoefficient(self, n, k):
		coeff = 1.0
		for x in range(n - k + 1, n + 1):
			coeff *= float(x)
		for x in range(1, k + 1):
			coeff /= float(x)

		return coeff

	def nextStep(self, priceBehaviour):

		prevIteration = self.iterations[len(self.iterations) - 1]
		prevS = prevIteration.S
		prevB = prevIteration.B
		n = len(self.iterations)
		ro = 1.0
		if priceBehaviour == SharePriceBehavior.DROP:
			ro = self.a

		elif priceBehaviour == SharePriceBehavior.RISE:
			ro = self.b

		currentS = self.SN(prevS, ro)
		currentB = self.BN(n)
		gamma = self.gamma_n(n + 1, currentS)
		betta = self.betta_n(n + 1, currentS)
		X = prevIteration.bettaNext * currentB + prevIteration.gammaNext * currentS
		description = ""
		if n == self.N:
			if X > 0:
				description += "Эмитент продаёт все имеющиеся акции по цене {currentS} " \
							   "на сумму {summa},\n выплачивает покупателю опциона X*={X}\n " \
							   "и на оставшиеся средства ({remainder}) возвращает долг ({debt})." \
					.format(currentS=currentS,
							summa=currentS * prevIteration.gammaNext,
							X=X,
							remainder=currentS * prevIteration.gammaNext - X,
							debt=currentB * prevIteration.bettaNext)
			else:
				description += "Эмитент должен продать все имеющиеся акции по цене {currentS} " \
							   "на сумму {summa} и вернуть долг. Покупатель опциона ничего не получает." \
					.format(currentS=currentS,
							summa=currentS * gamma)
		else:
			if gamma == 0.0 and betta == 0.0:
				description += "Эмитент должен продать все имеющиеся акции по цене {currentS} " \
							   "на сумму {summa} и вернуть долг. " \
							   "Покупатель опциона ничего не получает." \
					.format(currentS=currentS,
							summa=currentS * prevIteration.gammaNext)
			else:
				bettaChange = betta - prevIteration.bettaNext
				description += "Эмитент должен взять в долг ещё облигаций по цене {currentB}" \
							   " на сумму {summa} и купить на эти деньги акции по цене {currentS}" \
					.format(currentB=currentB,
							summa=-bettaChange * currentB,
							currentS=currentS)
		iterationValue = IterationValue(gamma, betta, ro, currentS, currentB, X, priceBehaviour, description)
		self.iterations.append(iterationValue)
		return iterationValue

	def getLastIteration(self):
		if(len(self.iterations) != 0):
			return self.iterations[len(self.iterations) - 1]
		else:
			return None

	def getIterationsCount(self):

		return len(self.iterations)

	def __str__(self):
		result = ""
		for val in self.iterations:
			result += str(val) + "\n"
		return result

	def get_html_text(self):
		result = ""
		for val in self.iterations:
			result += val.get_html_text() + "\n"
		return result




oc = OptionsComputing(-0.7, 0.1, 12.0, 15.0, 1.0, 180.0, 1)
print(oc)
print(oc.nextStep(SharePriceBehavior.RISE))
